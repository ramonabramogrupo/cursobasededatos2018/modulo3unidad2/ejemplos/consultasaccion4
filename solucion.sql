﻿/* 
  consultas de accion 4
*/

/* 
  Ejercicio 4
  definir claves ajenas 
*/

ALTER TABLE alquileres
  ADD CONSTRAINT FKAlquileresCliente 
  FOREIGN KEY (cliente) REFERENCES clientes(codigo)
  ON DELETE CASCADE ON UPDATE CASCADE; 

ALTER TABLE alquileres
  ADD CONSTRAINT FKAlquileresBicis 
  FOREIGN KEY (bici) REFERENCES bicis(codigo)
  ON DELETE CASCADE ON UPDATE CASCADE; 


ALTER TABLE averias
  ADD CONSTRAINT FKAveriasBicis 
  FOREIGN KEY (bici) REFERENCES bicis(codigo)
  ON DELETE CASCADE ON UPDATE CASCADE; 

/* 
  Ejercicio 6A
*/

  UPDATE 
    bicis b LEFT JOIN(SELECT bici,SUM(kms) n FROM alquileres GROUP BY bici) c1  
    on b.codigo = c1.bici
    SET b.kms=IFNULL(c1.n,0);

  /* otra opcion para arreglar los null */
  UPDATE 
      bicis
      SET kms=0
      WHERE kms IS NULL;

  /* Ejercicio 6b
    Bicis.años: calcular los años que tiene la bici a fecha de hoy. 
  */

    UPDATE bicis
      SET años=(year(NOW()) - year(fechaCompra));

    UPDATE bicis
      SET años=TRUNCATE((NOW() - fechaCompra)/365,0);      
    
    UPDATE bicis
      SET años=TIMESTAMPDIFF(year,fechaCompra,NOW());

    -- Ejercicio 6c

  UPDATE bicis b
    JOIN (SELECT a.bici,count(*) s FROM averias a
  GROUP BY a.bici) c1 ON b.codigo=c1.bici
    SET b.averias= c1.s;

--  d

--  Número de veces que se ha alquidalado esa bici
  UPDATE (SELECT
    bici,
    COUNT(*) n_alquileres
  FROM alquileres
  GROUP BY bici) c1
  JOIN bicis
    ON c1.bici = codigo
  SET bicis.alquileres = c1.n_alquileres;

  SELECT bici,SUM(coste) FROM averias GROUP BY bici;

 /* ejercicio 6e */
 
 UPDATE 
    (SELECT bici,SUM(coste) AS precio FROM averias GROUP BY bici) c1 
  JOIN bicis ON c1.bici=codigo 
  SET bicis.gastos=c1.precio;

/* F */

-- necesito calcular primero los descuentos y despues los preciosTotales

-- h

UPDATE 
  alquileres JOIN clientes c1 ON c1.codigo=alquileres.cliente 
  SET alquileres.descuento=c1.descuento;

-- I

UPDATE 
  alquileres JOIN bicis ON alquileres.bici = bicis.codigo
  SET precio_total=(bicis.precio *(1- alquileres.descuento));

-- F

  UPDATE (SELECT bici,SUM(precio_total) AS suma_bicis 
  FROM alquileres 
  GROUP BY bici) c1 
  RIGHT JOIN bicis ON c1.bici=codigo 
  SET bicis.beneficios=IFNULL(round(c1.suma_bicis,2),0);

/* g */

  UPDATE 
    (SELECT a.cliente,COUNT(*) n FROM alquileres a GROUP BY a.cliente) c1
    JOIN clientes c ON c.codigo=c1.cliente
    SET c.alquileres=c1.n;  
  
  
  
